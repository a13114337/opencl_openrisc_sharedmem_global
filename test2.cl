#include "../coreid.h"
__kernel void cl_kernel(__global int *size,
                       __global int *output,
                       __global int *inputA,
                       __global int *inputB) {
  int i = get_global_id(0);
  int n = (*size) ;
    for(; i<n; i++){
        output[i] = inputA[i] + inputB[i]  ;
    }  // for
}  // __kernel()
