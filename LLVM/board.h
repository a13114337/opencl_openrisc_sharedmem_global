#ifndef _BOARD_H_
#define _BOARD_H_

//INPUT CLK 25MHz
#define IN_CLK                  48000000

//UART
#define UART_BAUD_RATE          115200
#define UART_DEVISOR            (IN_CLK/(16*UART_BAUD_RATE))
#define UART_BASE               0x20000000
#define UART_IRQ                1

//GPIO
#define GPIO_BASE               0x10000000
#define RGPIO_IN                (GPIO_BASE + 0x0)
#define RGPIO_OUT               (GPIO_BASE + 0x4)
#define RGPIO_OE                (GPIO_BASE + 0x8)
#define RGPIO_INTE              (GPIO_BASE + 0xc)
#define RGPIO_PTRIG             (GPIO_BASE + 0x10)
#define RGPIO_AUX               (GPIO_BASE + 0x14)
#define RGPIO_CTRL              (GPIO_BASE + 0x18)
#define RGPIO_INTS              (GPIO_BASE + 0x1c)
#define RGPIO_ECLK              (GPIO_BASE + 0x20)
#define RGPIO_NEC               (GPIO_BASE + 0x24)
#define GPIO_IRQ                0


//#define REG8(add)               *((volatile unsigned char *)(add))
//#define REG16(add)              *((volatile unsigned short *)(add))
//#define REG32(add)              *((volatile unsigned long *)(add))

#endif

