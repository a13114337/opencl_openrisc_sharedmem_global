	.file	"or1200_sopc.c"
	.section .text
	.align	4
.proc	_main
	.global _main
	.type	_main, @function
_main:
.LFB0:
	l.sw    	-8(r1),r2	 # SI store
.LCFI0:
	l.addi  	r2,r1,0
.LCFI1:
	l.sw    	-4(r1),r9	 # SI store
.LCFI2:
	l.addi	r1,r1,-524	# allocate frame
.LCFI3:
	l.addi  	r3,r0,128	 # move immediate I
	l.sw    	-12(r2),r3	 # SI store
	l.addi  	r3,r2,-12
	l.addi  	r4,r2,-524
	l.jal   	_test
	l.nop			# nop delay slot
	l.addi  	r3,r0,0	 # move immediate I
	l.ori   	r11,r3,0	 # move reg to reg
	l.ori	r1,r2,0	# deallocate frame
	l.lwz   	r2,-8(r1)	 # SI load
	l.lwz   	r9,-4(r1)	 # SI load
	l.jr    	r9
	l.nop			# nop delay slot
.LFE0:
	.size	_main, .-_main
	.align	4
.proc	_test
	.global _test
	.type	_test, @function
_test:
.LFB1:
	l.sw    	-4(r1),r2	 # SI store
.LCFI4:
	l.addi  	r2,r1,0
.LCFI5:
	l.addi	r1,r1,-16	# allocate frame
.LCFI6:
	l.sw    	-12(r2),r3	 # SI store
	l.sw    	-16(r2),r4	 # SI store
	l.addi  	r3,r0,0	 # move immediate I
	l.sw    	-8(r2),r3	 # SI store
	l.j     	.L3
	l.nop			# nop delay slot
.L4:
	l.lwz   	r3,-8(r2)	 # SI load
	l.slli  	r3,r3,2
	l.lwz   	r4,-16(r2)	 # SI load
	l.add   	r3,r4,r3
	l.lwz   	r4,-8(r2)	 # SI load
	l.slli  	r4,r4,2
	l.lwz   	r5,-16(r2)	 # SI load
	l.add   	r4,r5,r4
	l.lwz   	r4,0(r4)	 # SI load
	l.addi  	r4,r4,21
	l.sw    	0(r3),r4	 # SI store
	l.lwz   	r3,-8(r2)	 # SI load
	l.addi  	r3,r3,1
	l.sw    	-8(r2),r3	 # SI store
.L3:
	l.lwz   	r3,-12(r2)	 # SI load
	l.lwz   	r4,0(r3)	 # SI load
	l.lwz   	r3,-8(r2)	 # SI load
	l.sfgts 	r4,r3
	l.bf	.L4
	l.nop			# nop delay slot
	l.ori	r1,r2,0	# deallocate frame
	l.lwz   	r2,-4(r1)	 # SI load
	l.jr    	r9
	l.nop			# nop delay slot
.LFE1:
	.size	_test, .-_test
	.section	.debug_frame,"",@progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.string	""
	.uleb128 0x1
	.sleb128 -4
	.byte	0x9
	.byte	0xc
	.uleb128 0x1
	.uleb128 0x0
	.align	4
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0x82
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI3-.LCFI1
	.byte	0x89
	.uleb128 0x1
	.align	4
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI4-.LFB1
	.byte	0x82
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI5-.LCFI4
	.byte	0xd
	.uleb128 0x2
	.align	4
.LEFDE2:
	.ident	"GCC: (OpenRISC 32-bit toolchain for or32-elf (built 20120328 based on OpenCores SVN repository revision 789)) 4.5.1-or32-1.0rc4"
