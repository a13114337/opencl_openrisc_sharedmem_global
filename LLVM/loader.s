	.file	"loader.c"
	.section .text
	.align	4
.proc	_main
	.global _main
	.type	_main, @function
_main:
.LFB0:
	l.sw    	-4(r1),r2	 # SI store
.LCFI0:
	l.addi  	r2,r1,0
.LCFI1:
	l.addi	r1,r1,-520	# allocate frame
.LCFI2:
	l.addi  	r3,r0,128	 # move immediate I
	l.sw    	-8(r2),r3	 # SI store
	l.addi  	r3,r2,-8
	l.movhi 	r4,hi(524288)	 # move immediate M
	l.ori   	r4,r4,5632
	l.movhi 	r5,hi(524288)	 # move immediate M
	l.ori   	r5,r5,1536
	l.movhi 	r6,hi(524288)	 # move immediate M
	l.ori   	r6,r6,3584
#APP
# 4 "./LLVM/loader.c" 1
	l.add      r3,r0,r3
	l.add      r4,r0,r4
	l.add      r5,r0,r5
	l.add      r6,r0,r6
	l.jal    cl_kernel
	l.nop    
	l.jr r0      
	l.nop        

# 0 "" 2
#NO_APP
	l.addi  	r3,r0,0	 # move immediate I
	l.ori   	r11,r3,0	 # move reg to reg
	l.ori	r1,r2,0	# deallocate frame
	l.lwz   	r2,-4(r1)	 # SI load
	l.jr    	r9
	l.nop			# nop delay slot
.LFE0:
	.size	_main, .-_main
	.section	.debug_frame,"",@progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.string	""
	.uleb128 0x1
	.sleb128 -4
	.byte	0x9
	.byte	0xc
	.uleb128 0x1
	.uleb128 0x0
	.align	4
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0x82
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0xd
	.uleb128 0x2
	.align	4
.LEFDE0:
	.ident	"GCC: (OpenRISC 32-bit toolchain for or32-elf (built 20120328 based on OpenCores SVN repository revision 789)) 4.5.1-or32-1.0rc4"
