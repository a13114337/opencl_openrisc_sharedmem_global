# README #

This README would normally document whatever steps are necessary to get your application up and running.


### How do I get set up? ###

* Prepare OpenCL SDK platform, FPGA V7
* sudo sh do.sh
* host program in hello.cpp
* kernel task in test2.cl

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### environment setup ###
* Please install these toolchain

1. clang-or1k-master
(git clone git://github.com/openrisc/llvm-or1k.git)  
2. llvm-or1k-master
(git clone git://github.com/openrisc/clang-or1k.git)
3. openrisc-toolchain-ocsvn-rev789
(wget ftp://ocuser:ocuser@openrisc.opencores.org/toolchain/openrisc-toolchain-ocsvn-rev789.tar.bz2)