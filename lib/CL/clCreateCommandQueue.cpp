#include "./device/device.h"

CL_API_ENTRY cl_command_queue CL_API_CALL
clCreateCommandQueue(cl_context                     context, 
                     cl_device_id                   device, 
                     cl_command_queue_properties    properties,
                     cl_int *                       errcode_ret)
{   
  cl_command_queue cq_temp = new _cl_command_queue;
  if(context == NULL){
    *errcode_ret = CL_INVALID_CONTEXT;
    return NULL;
  }
  if(device == NULL){
    *errcode_ret = CL_INVALID_DEVICE;
    return NULL;
  }
  if( properties & CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE ){
    printf("Not implemented yet!");
  }
  if( properties & CL_QUEUE_PROFILING_ENABLE ){
    printf("Not implemented yet!");
  }
	
  *errcode_ret = CL_SUCCESS;
  cq_temp->context=context;
  cq_temp->refcount=1;
  cq_temp->device_id = device;
  cq_temp->properties = properties;

  return cq_temp;
}
