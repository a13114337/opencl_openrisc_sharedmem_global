#include "./device/device.h"
#include "manage_func.h"

CL_API_ENTRY cl_int CL_API_CALL
clGetDeviceIDs(cl_platform_id   platform,
               cl_device_type   device_type, 
               cl_uint          num_entries, 
               cl_device_id*     devices, 
               cl_uint *        num_devices)
{
  static bool flag[DEVICENUM] = {0};//flag[i]=1 means the ith device is available
  if(platform == NULL)
    return CL_INVALID_VALUE;
  
  if(devices == NULL && num_devices != NULL)
    *num_devices = check_dev(flag);
  else if(devices != NULL && num_entries >= 1)
    dev_service(devices,flag,platform); 

  return CL_SUCCESS;
}
