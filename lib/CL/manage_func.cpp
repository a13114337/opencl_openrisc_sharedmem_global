#include "manage_func.h"
#include <string.h>

//store string type info function
int manage_string(char * str,	
		  size_t param_value_size,  
		  void *param_value,  
		  size_t *param_value_size_ret)
{
  size_t length = strlen(str) + 1;
  if (param_value_size < length)
    return CL_INVALID_VALUE;
  else
  {
    *param_value_size_ret = length;
    memcpy(param_value,str,length);
    return CL_SUCCESS;
  }
}

//return available device number,flag[i]=1 means the ith device is available
cl_uint check_dev(bool* flag)
{
  cl_uint num=0;
  if(/*device0_open*/ true){
    num+=1;
    flag[0]=1;
  }

  if(/*device1_open ==*/ true){
    num+=1;
    flag[1]=1;
  }
  if(/*device2_open ==*/ true){
    num+=1;
    flag[2]=1;
  }
  return num;
}


//read info from file and store in device[i]
void dev_service(cl_device_id* device,
		 bool* flag,
		 cl_platform_id   platform){
  FILE *fp;
  fp = fopen("./device_info.txt","r");
  if(fp == NULL)
    printf("open file error : ");
  int i=0;//while loop
  int j=0;// 2 for loops in while
  while(i<MAX_INFONUM){
    if(flag[i]==true){
      cl_device_id temp_device = new _cl_device_id;
      //there are MAX_INFONUM ctmp,each can contain 30 characters
      //using new because we need a specific memory space to store the strings read from files
      //following 3 lines are dynamically declaring a char[MAX_INFONUM][MAX_INFOLENGTH]
      char** ctmp = new char*[MAX_INFONUM];
      for(j=0;j<MAX_INFONUM;j++)
	ctmp[j] = new char[MAX_INFOLENGTH+1];
      
      fscanf(fp,"%s",ctmp[0]);
      
      temp_device->dev_platform = platform;   
      
      for(j=0;j<MAX_INFONUM;j++)
	fscanf(fp,"%s",ctmp[j]);	
      
      if(strcmp(ctmp[0],"CL_DEVICE_TYPE_CPU")== 0){
	temp_device->dev_type = CL_DEVICE_TYPE_CPU;
      }
      else if(strcmp(ctmp[0],"CL_DEVICE_TYPE_GPU")== 0){
	temp_device->dev_type = CL_DEVICE_TYPE_GPU;
      }
      temp_device->dev_vendor_id = (cl_uint)atoi(ctmp[1]);
      temp_device->dev_name = ctmp[2];
      temp_device->dev_vendor = ctmp[3];
      temp_device->dev_driver_version = ctmp[4];
      temp_device->dev_profile = ctmp[5];
      temp_device->dev_version = ctmp[6];  
      
      device[i]=temp_device;
    }
    i++;
  }
}

