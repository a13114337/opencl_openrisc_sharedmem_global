#include "./device/device.h"

CL_API_ENTRY cl_int CL_API_CALL
clGetPlatformIDs(cl_uint          num_entries,
                 cl_platform_id * platforms,
                 cl_uint *        num_platforms)
{		
	if( ( (num_platforms != NULL) && (platforms == NULL) ) )
		*num_platforms = 1;	// MJ-openrisc is the only one

	// create platform_id 
	else if( ( (platforms != NULL) && (num_entries > 0) ) ){
		cl_platform_id temp = new _cl_platform_id;
		temp->id = 1;
		*platforms = temp;		
	}
	else{
		return CL_INVALID_VALUE;
	}
	return CL_SUCCESS;
}

