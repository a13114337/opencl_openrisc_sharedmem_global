#include "./device/device.h"

CL_API_ENTRY cl_program CL_API_CALL
clCreateProgramWithSource(cl_context        context,
                          cl_uint           count,
                          const char **     strings,
                          const size_t *    lengths,
                          cl_int *          errcode_ret)
{
	// in case, there so many source file BUT NOW is only one ; source_file_name => *string[count]
	
	// openrisc special case!!
	if(context != NULL){
		//system("sudo mkdir .kernel_build");
		FILE *fp;
		fp = fopen("./.kernel_build/kernel.cl","w");
		fwrite( *strings, *lengths, 1, fp );
		fclose(fp);	
		//system("sudo source path");
		//system("rm ./.kernel_build/ -r");
		// mkdir ./.kernel_build
		// cp hello.cl to dir
		//system("sudo cp ./test.cl ./.kernel_build");
		// clang hello.cl to hello.bc
		system("sudo clang -c ./.kernel_build/kernel.cl -emit-llvm -o ./.kernel_build/kernel.bc");
		// llc hello.bc to hello.s
		system("sudo llc -march=or1k ./.kernel_build/kernel.bc -o ./.kernel_build/kernel.s");
		
		// **temp** using gcc and main function
//or32-elf-gcc -fleading-underscore -S or1200_sopc.c
//or32-elf-gcc -fleading-underscore -c or1200_sopc.c -o or1200_sopc.o

		system("sudo cp ./.kernel_build/kernel.s ./LLVM");
		system("sudo /opt/openrisc/toolchain/bin/or32-elf-as ./LLVM/reset.S -o ./LLVM/reset.o");
		system("sudo /opt/openrisc/toolchain/bin/or32-elf-as ./LLVM/kernel.s -o ./LLVM/kernel.o");
		system("sudo /opt/openrisc/toolchain/bin/or32-elf-gcc -fleading-underscore -S ./LLVM/loader.c -o ./LLVM/loader.s");
		system("sudo /opt/openrisc/toolchain/bin/or32-elf-as  ./LLVM/loader.s -o ./LLVM/loader.o");
		system("sudo /opt/openrisc/toolchain/bin/or32-elf-ld -T ./LLVM/ram.ld ./LLVM/reset.o ./LLVM/kernel.o ./LLVM/loader.o -o ./LLVM/or1200_sopc.or32");

		//system("objdump -D ./LLVM/or1200_sopc.or32 > ./LLVM/or1200.dis");
		system("sudo /opt/openrisc/toolchain/bin/or32-elf-objcopy -O ihex ./LLVM/or1200_sopc.or32 ./LLVM/or1200_sopc.ihex");
		system("sudo ./LLVM/ihex2mif -f ./LLVM/or1200_sopc.ihex -o ./LLVM/ram0.mif");
		//system("cd ..");
		system("sudo cp ./LLVM/ram0.mif ./");

		return NULL;
	}
	return NULL;
}
