#include "./device/device.h"

CL_API_ENTRY cl_mem CL_API_CALL
clCreateBuffer(cl_context   context,
               cl_mem_flags flags,
               size_t       size,
               void *       host_ptr,
               cl_int *     errcode_ret)
{
	// out of memory 0x6000-0x8000
	if(size > 2000){
		*errcode_ret = CL_OUT_OF_RESOURCES;
		return NULL;
	}
	cl_mem mem_temp = new _cl_mem;
	// openrisc special case!!
	if(context != NULL && flags != CL_MEM_READ_WRITE ){
		mem_temp->context = context; 
		mem_temp->flag = flags;
		mem_temp->using_size = size;
		return mem_temp;
	}
	return NULL;
}
