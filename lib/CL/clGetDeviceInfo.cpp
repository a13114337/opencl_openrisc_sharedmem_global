#include "./device/device.h"
#include "./manage_func.h"

CL_API_ENTRY cl_int CL_API_CALL
clGetDeviceInfo(cl_device_id    device,
                cl_device_info  param_name, 
                size_t          param_value_size, 
                void *		param_value,
                size_t *	param_value_size_ret)
{
  cl_int ret;

  if(device == NULL)
    return CL_INVALID_DEVICE;
  if (!param_value)
    return CL_INVALID_VALUE;
  if(!param_value_size_ret)
    return CL_INVALID_VALUE;


  switch(param_name) {
  case CL_DEVICE_TYPE:
    ret = manage(device->dev_type,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_VENDOR_ID:
    ret = manage(device->dev_vendor_id,param_value_size,param_value,param_value_size_ret); 
    break;
  case CL_DEVICE_PLATFORM:
     if (param_value_size < sizeof(cl_platform_id))
    return CL_INVALID_VALUE;
  else
  {
    *param_value_size_ret = sizeof(cl_platform_id);
    *((cl_platform_id*)param_value) = device->dev_platform;
    return CL_SUCCESS;
  }

    break;
  case CL_DEVICE_NAME: 
    ret = manage_string(device->dev_name,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_VENDOR: 
    ret = manage_string(device->dev_vendor,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DRIVER_VERSION:
    ret = manage_string(device->dev_driver_version,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_PROFILE:
    ret = manage_string(device->dev_profile,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_VERSION:
    ret = manage_string(device->dev_version,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_EXTENSIONS:
    ret = manage_string(device->dev_extensions,param_value_size,param_value,param_value_size_ret); 
    break;
  case CL_DEVICE_MAX_COMPUTE_UNITS:
    ret = manage(device->dev_max_compute_unit,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS:
    ret = manage(device->dev_max_work_dim,param_value_size,param_value,param_value_size_ret); 
    break;
  case CL_DEVICE_MAX_WORK_ITEM_SIZES:
    if(param_value_size < 3*sizeof(size_t)) 
      return CL_INVALID_VALUE;
    else{
      ((size_t*)param_value)[0] = device->dev_max_work_item_size[0];
      ((size_t*)param_value)[1] = device->dev_max_work_item_size[1];
      ((size_t*)param_value)[2] = device->dev_max_work_item_size[2];
      *param_value_size_ret = 3*sizeof(cl_int);
    }
    break;
  case CL_DEVICE_MAX_WORK_GROUP_SIZE:
    ret = manage(device->dev_max_work_group_size,param_value_size,param_value,param_value_size_ret); 
    break;
  case CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR:
    ret = manage(device->dev_prefer_vector_width_char,param_value_size,param_value,param_value_size_ret); 
    break;
  case CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT:
    ret = manage(device->dev_prefer_vector_width_short,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT:
    ret = manage(device->dev_prefer_vector_width_int,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG:
    ret = manage(device->dev_prefer_vector_width_long,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT:
    ret = manage(device->dev_prefer_vector_width_float,param_value_size,param_value,param_value_size_ret); 
    break;
  case CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE:
    ret = manage(device->dev_prefer_vector_width_double,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF:
    ret = manage(device->dev_prefer_vector_width_half,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_MAX_CLOCK_FREQUENCY:
    ret = manage(device->dev_max_clock_frequency,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_ADDRESS_BITS:
    ret = manage(device->dev_address_bits,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_MAX_MEM_ALLOC_SIZE:
    ret = manage(device->dev_max_mem_alloc_size,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE:
    ret = manage(device->dev_min_data_type_align_size,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_IMAGE_SUPPORT: 
    ret = manage(device->dev_image_support,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_MAX_READ_IMAGE_ARGS:
    ret = manage(device->dev_max_read_image_args,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_MAX_WRITE_IMAGE_ARGS:
    ret = manage(device->dev_max_write_image_args,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_IMAGE2D_MAX_WIDTH:
    ret = manage(device->dev_image2D_max_width,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_IMAGE2D_MAX_HEIGHT:
    ret = manage (device->dev_image2D_max_height,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_IMAGE3D_MAX_WIDTH:
    ret = manage (device->dev_image3D_max_width,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_IMAGE3D_MAX_HEIGHT:
    ret = manage (device->dev_image3D_max_height,param_value_size,param_value,param_value_size_ret);
    break;	
  case CL_DEVICE_IMAGE3D_MAX_DEPTH:
    ret = manage(device->dev_image3D_max_depth,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_MAX_SAMPLERS:
    ret = manage(device->dev_max_samplers,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_MAX_PARAMETER_SIZE:
    ret = manage(device->dev_max_parameter_size,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_MEM_BASE_ADDR_ALIGN:
    ret = manage(device->dev_mem_base_addr_align,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_SINGLE_FP_CONFIG:
    ret = manage(device->dev_single_fp_config,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE:
    ret = manage(device->dev_max_constant_mem_buf_size,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_LOCAL_MEM_TYPE:
    ret = manage(device->dev_local_mem_type,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_LOCAL_MEM_SIZE:
    ret = manage(device->dev_local_mem_size,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_GLOBAL_MEM_SIZE:
    ret = manage(device->dev_global_mem_size,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_ERROR_CORRECTION_SUPPORT:
    ret = manage(device->dev_error_correction_support,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_DEVICE_QUEUE_PROPERTIES:
    ret = manage(device->dev_queue_properties,param_value_size,param_value,param_value_size_ret);
    break;
  
  default:
    printf("Get Device Information 0x%X is not implemented", param_name);
    return CL_INVALID_PLATFORM;	
    break;
  }
  
  return ret;
}
