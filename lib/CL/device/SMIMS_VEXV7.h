//
//	VEXV7 SDK Header File
//
// 2013.09.17 SMIMS ikki create
// 2014.06.30 SMIMS ikki add Wait_ReadReady and Wait_WriteReady

#include "SMIMS_define.h"

bool SMIMS_VEXV7_ProgramFPGA(int iBoard, char * BitFile);
bool SMIMS_VEXV7_ProgramFPGAFromSD(int iBoard, int iBitFileIndex); 
bool SMIMS_VEXV7_ARMOpen(int iBoard, char * SerialNO, float freq);
bool SMIMS_VEXV7_AppOpen(int iBoard, char * SerialNO, float freq);
bool SMIMS_VEXV7_AppFIFOReadData(int iBoard, WORD *Buffer, unsigned size);
bool SMIMS_VEXV7_AppFIFOWriteData(int iBoard, WORD *Buffer, unsigned size);
bool SMIMS_VEXV7_AppChannelSelector(int iBoard, BYTE channel);
bool SMIMS_VEXV7_Wait_ReadReady(int iBoard, unsigned TimeOut);
bool SMIMS_VEXV7_Wait_WriteReady(int iBoard, unsigned TimeOut);
bool SMIMS_VEXV7_AppClose(int iBoard);
char* SMIMS_VEXV7_GetLastErrorMsg(int iBoard);

//SDK2MEM Read/Write
bool SMIMS_VEXV7_SDK2MEM_Read(int iBoard, WORD Addr, WORD *Buffer, unsigned size);
bool SMIMS_VEXV7_SDK2MEM_Write(int iBoard, WORD Addr, WORD *Buffer, unsigned size);
bool SMIMS_VEXV7_SDK2MEM_WaitReady(int iBoard, WORD Mask,  unsigned TimeOut, bool & bResult);

