#include "./device/device.h"

CL_API_ENTRY cl_int CL_API_CALL
clReleaseContext(cl_context  context){  
  if (context == NULL)
    return CL_INVALID_CONTEXT;
  else
    context->refcount--;
  return CL_SUCCESS;
}
