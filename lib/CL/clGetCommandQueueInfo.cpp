#include "./device/device.h"
#include "./manage_func.h"

CL_API_ENTRY cl_int CL_API_CALL
clGetCommandQueueInfo(cl_command_queue		queue,
                      cl_command_queue_info	param_name,
                      size_t			param_value_size,
                      void *			param_value,
                      size_t *			param_value_size_ret){

  cl_int ret;

  if (queue == NULL)
    return CL_INVALID_CONTEXT;
  if (!param_value)
    return CL_INVALID_VALUE;
  if(!param_value_size_ret)
    return CL_INVALID_VALUE;

  switch(param_name){
  case CL_QUEUE_CONTEXT:
    ret = manage(queue->context,param_value_size,param_value,param_value_size_ret);
    break;
  case  CL_QUEUE_DEVICE :
    ret = manage(queue->device_id,param_value_size,param_value,param_value_size_ret);
    break;
  case  CL_QUEUE_REFERENCE_COUNT :
    ret = manage(queue->refcount,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_QUEUE_PROPERTIES:
    ret = manage(queue->properties,param_value_size,param_value,param_value_size_ret);
    
    break;
  }
  return ret;
}


 

