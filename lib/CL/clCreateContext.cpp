#include "./device/device.h"

CL_API_ENTRY cl_context CL_API_CALL
clCreateContext(const cl_context_properties * properties,
                cl_uint                 num_devices,
                const cl_device_id *    devices,
                void (CL_CALLBACK * pfn_notify)
		(const char *, const void *, size_t, void *),
                void *                  user_data,
                cl_int *                errcode_ret)
{
  cl_context context_temp = new _cl_context;
  
  // openrisc special case!!
  if(devices != NULL && num_devices == 1){
    context_temp->device_id = *devices;
    context_temp->refcount = 1;
    context_temp->num_devices = 1;
  //set properties
    context_temp->context_properties[0] = properties[0];
    context_temp->context_properties[1] = 1;
    context_temp->context_properties[2] = properties[2];


    return context_temp;
  }
  else
    *errcode_ret = CL_INVALID_VALUE;

  return NULL;
}
