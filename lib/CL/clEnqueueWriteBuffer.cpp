#include "./device/device.h"

CL_API_ENTRY cl_int CL_API_CALL
clEnqueueWriteBuffer (	cl_command_queue command_queue,
                        cl_mem buffer,
 	                      cl_bool blocking_write,
 	                      size_t offset,
 	                      size_t size,
 	                      const void *ptr,
 	                      cl_uint num_events_in_wait_list,
 	                      const cl_event *event_wait_list,
 	                      cl_event *event)
{

  Baton_Send(core_id_recieve, size, offset, (unsigned short *)ptr);
	return 0;
}
