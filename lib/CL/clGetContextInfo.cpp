#include "./device/device.h"
#include "./manage_func.h"

CL_API_ENTRY cl_int CL_API_CALL
clGetContextInfo(cl_context         context, 
                 cl_context_info    param_name, 
                 size_t             param_value_size, 
                 void *		    param_value, 
                 size_t *	    param_value_size_ret){

  cl_int ret;

  if (context == NULL)
    return CL_INVALID_CONTEXT;
  if (!param_value)
    return CL_INVALID_VALUE;
  if(!param_value_size_ret)
    return CL_INVALID_VALUE;

  switch(param_name){
  case CL_CONTEXT_REFERENCE_COUNT:
    ret = manage(context->refcount,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_CONTEXT_NUM_DEVICES:
    ret = manage(context->num_devices,param_value_size,param_value,param_value_size_ret);
    break;
  case CL_CONTEXT_PROPERTIES:
    ret = manage(context->context_properties[1],param_value_size,param_value,param_value_size_ret);
    break;
  case CL_CONTEXT_DEVICES:
    ret = manage(context->device_id,param_value_size,param_value,param_value_size_ret);	
    break;
  }
  return ret;
}


 
