#include "./device/device.h"

CL_API_ENTRY cl_int CL_API_CALL
clEnqueueTask(cl_command_queue  command_queue,
              cl_kernel         kernel,
              cl_uint           num_events_in_wait_list,
              const cl_event *  event_wait_list,
              cl_event *        event)
{

	Baton_Start(0);
	Baton_Start(1);
  Baton_Start(2);
  Baton_Start(3);
	Baton_UART();	//Before dumping data, you must do this function, although there isn't any UART_print.

	return 0;
}
