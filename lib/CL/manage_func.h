#ifndef MFUNC_H
#define MFUNC_H

#include "./device/device.h"

#define DEVICENUM 5
#define MAX_INFONUM   7
#define MAX_INFOLENGTH   50

template<class T>
int manage(T data,
	 size_t param_value_size,  
	 void *param_value,  
	 size_t *param_value_size_ret)
{
  size_t length = sizeof(T);
  if (param_value_size < length)
    return CL_INVALID_VALUE;
  else
  {
    *param_value_size_ret = length;
    *((T*)param_value) = data;
    return CL_SUCCESS;
  }
}

int manage_string(char * str,	
		  size_t param_value_size,  
		  void *param_value,  
		  size_t *param_value_size_ret);

cl_uint check_dev(bool* flag);

void dev_service(cl_device_id* devices,
		 bool *flag,
		 cl_platform_id platform);




#endif




