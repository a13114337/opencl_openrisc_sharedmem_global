#include "./device/device.h"
#include "./manage_func.h"

CL_API_ENTRY cl_int CL_API_CALL
clGetPlatformInfo(cl_platform_id platform,  
  		  cl_platform_info param_name,  
		  size_t param_value_size,  
		  void *param_value,  
		  size_t *param_value_size_ret) {
  cl_int ret;

  if (platform == NULL)
    return CL_INVALID_PLATFORM;
  if (!param_value)
    return CL_INVALID_VALUE;
  if(!param_value_size_ret)
    return CL_INVALID_VALUE;


  switch(param_name){
  case CL_PLATFORM_PROFILE:
    ret = manage_string("FULL_PROFILE",param_value_size,param_value,param_value_size_ret);
    break;
  case CL_PLATFORM_VERSION:
    ret = manage_string("OpenCL 1.2",param_value_size,param_value,param_value_size_ret);
    break;
  case CL_PLATFORM_NAME:
    ret = manage_string("OpenRisc OpenCL",param_value_size,param_value,param_value_size_ret);
    break;
  case CL_PLATFORM_VENDOR:
    ret = manage_string("MultiJerry",param_value_size,param_value,param_value_size_ret);
    break;
  }
  return ret;
}
