#ifndef OR_H
#define OR_H

#include "CL/opencl.h"
#include <string>

struct _cl_device_id{
	//cl_platform_id platform_id;
	//cl_int id;
	//cl_int num_device;

	
	/* Device Info. */
	/* Device basic information */
	cl_device_type	dev_type;
	cl_uint			dev_vendor_id;
	cl_platform_id	dev_platform;
	char*			dev_name;
	char*			dev_vendor;
	char*			dev_driver_version;
	char*			dev_profile;
	char*			dev_version;
	
	char*			dev_extensions;
	
	
	/* Device task schedule information */
	cl_uint			dev_max_compute_unit;
	cl_uint			dev_max_work_dim;
	size_t*			dev_max_work_item_size;
	size_t			dev_max_work_group_size;
	
	/* Device data type  information (prefer & native) */
	cl_uint			dev_prefer_vector_width_char;
	cl_uint			dev_prefer_vector_width_short;
	cl_uint			dev_prefer_vector_width_int;
	cl_uint			dev_prefer_vector_width_long;
	cl_uint			dev_prefer_vector_width_float;
	cl_uint			dev_prefer_vector_width_double;
	cl_uint			dev_prefer_vector_width_half;

	/* Device HW setting information */
	cl_uint			dev_max_clock_frequency;
	cl_uint			dev_address_bits;
	cl_ulong		dev_max_mem_alloc_size;
	cl_uint			dev_min_data_type_align_size;
	
	/* Device Image Supported information */
	cl_bool			dev_image_support;
	cl_uint			dev_max_read_image_args;
	cl_uint			dev_max_write_image_args;
	size_t			dev_image2D_max_width;
	size_t			dev_image2D_max_height;
	size_t			dev_image3D_max_width;
	size_t			dev_image3D_max_height;
	size_t			dev_image3D_max_depth;
	
	
	/* Device kernel execution information */
	cl_uint			dev_max_samplers;
	size_t			dev_max_parameter_size;
	cl_uint			dev_mem_base_addr_align;
	cl_device_fp_config	dev_single_fp_config;
	cl_ulong		dev_max_constant_mem_buf_size;
	cl_device_local_mem_type dev_local_mem_type;
	cl_ulong		dev_local_mem_size;
	cl_ulong		dev_global_mem_size;
	cl_bool			dev_error_correction_support;
	cl_command_queue_properties	dev_queue_properties;

	/*==============*/
};



struct _cl_context{
	cl_device_id device_id;
	cl_int refcount;
	cl_int num_devices;
	cl_context_properties context_properties[3]; 
	void * userdata;
};

struct _cl_command_queue{
	cl_context context;
	cl_device_id device_id;
	cl_command_queue_properties properties;
	cl_int refcount;
};

struct _cl_mem{
	cl_context context;
	cl_mem_flags flag;
	size_t using_size;
};

struct _cl_program{

};

struct _cl_kernel{

};

struct _cl_platform_id{
	cl_int id;
};

#endif // end or.h
