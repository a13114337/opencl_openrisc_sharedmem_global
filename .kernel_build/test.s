	.file	"./.kernel_build/test.bc"
	.text
	.globl	test
	.align	4
	.type	test,@function
test:                                   # @test
# BB#0:                                 # %entry
	l.movhi	r5, 0
	l.lwz	r6, 0(r3)
	l.sfltsi	r6, 1
	l.bf	.LBB0_2
	l.nop	0
.LBB0_1:                                # %for.body
                                        # =>This Inner Loop Header: Depth=1
	l.slli	r6, r5, 2
	l.add	r6, r4, r6
	l.movhi	r7, 4660
	l.ori	r7, r7, 22136
	l.lwz	r8, 0(r6)
	l.add	r7, r8, r7
	l.sw	0(r6), r7
	l.addi	r5, r5, 1
	l.lwz	r6, 0(r3)
	l.sflts	r5, r6
	l.bf	.LBB0_1
	l.nop	0
.LBB0_2:                                # %for.end
	l.jr	r9
	l.nop	0
.Ltmp0:
	.size	test, .Ltmp0-test


