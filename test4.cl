
__kernel void cl_kernel(__global int *size, __global int* buf)
{
  int i;
	
	for(i=0; i<*size ; i++)
	{
	   buf[i] = buf[i] + 4;	
	}
}

/*
#include "../coreid.h"
__kernel void cl_kernel(__global int *size, __global int* buf)
{
  int i = get_global_id(0);
  int n = i + (*size)/2;
    
    for(; i<n; i++){
	   buf[i] = buf[i] + 4;	
	}
}
  */
