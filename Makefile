CC= g++
CFLAGS= -w -g
LIBES= -L./
MaCube_SMIMSLIB= -lmyCL -lsmimsvexv7 -static -lpthread -lrt

INCLUDES= -I ./
SOURCES=HostProgram.cpp
OBJECTS=$(SOURCES:.cpp=.o)
MaCube_TARGET= ./run


all : $(MaCube_TARGET)

$(MaCube_TARGET) : $(OBJECTS)
	@$(CC) $(CFLAGS) $(OBJECTS) $(LIBES) $(INCLUDES) $(MaCube_SMIMSLIB) -o $(MaCube_TARGET)

$(OBJECTS):$(SOURCES)
	@$(CC) $(CFLAGS) -c -o $@ $(INCLUDES) $(SOURCES)

clean:
	@rm -rf *.o
	@rm -rf $(MaCube_TARGET)
	rm libmyCL.a

cl_Lib:
	g++ $(CFLAGS) -c ./lib/CL/*.cpp
	g++ $(CFLAGS) -c ./lib/CL/device/*.cpp
	ar rcs libmyCL.a *.o
	rm *.o
