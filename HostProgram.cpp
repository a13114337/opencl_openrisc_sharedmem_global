/* ============================================================
//--cambine: kernel funtion of Breadth-First-Search
//--author:	created by Jianbin Fang
//--date:	06/12/2010
============================================================ */

#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include "./lib/CL/device/device.h"


#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include "./include/CL/cl.h"
#endif

#define MEM_SIZE (128)
#define MAX_SOURCE_SIZE (0x1000)

int core_id_recieve;	//determine which core to receive, declared as extern in device.h in order to let clEnqueueReadBuffer knows its value

int main()
{
/*=============================================================
  time measurement variables
===============================================================*/
  struct timeval start;
  struct timeval end;
  double exec_time;

  unsigned int A [MEM_SIZE*4] ;
  unsigned int B [MEM_SIZE*4] ;

  for ( int i = 0 ; i < MEM_SIZE*4 ; i++ ) {
     A[i] = 1 ;
     B[i] = i*2 ;
  }

  // ---

  FILE *fp2;	//open coreid.h

  cl_device_id *device_id = NULL;
  cl_context context = NULL;
  cl_command_queue command_queue = NULL;
  cl_mem memobj = NULL;
  cl_program program = NULL;
  cl_kernel kernel = NULL;
  cl_platform_id *platform_id = NULL;
  cl_uint ret_num_devices;
  cl_uint ret_num_platforms;
  cl_int ret;

  size_t size = 0;

/*=============================================================
  variables to receive data
  string: output
  ReadBuffer: recieve core 0 data
  ReadBuffer2: recieve core 1 data
===============================================================*/
  int string[MEM_SIZE];
  unsigned short ReadBuffer[MEM_SIZE*4/sizeof(unsigned short)];
  unsigned short ReadBuffer2[MEM_SIZE*4/sizeof(unsigned short)];

  int i;
  for(i=0; i<MEM_SIZE; i++)
	string[i] = 0;
/*=============================================================
  open OpenCL kernel file
  => read all code in file and save it to source_str
===============================================================*/
  FILE *fp;
  char fileName[] = "test2.cl";
  char *source_str;
  size_t source_size;

  fp = fopen(fileName, "r");
  if (!fp) {
    fprintf(stderr, "Failed to load kernel.\n");
    exit(1);
  }
  source_str = (char*)malloc(MAX_SOURCE_SIZE);
  source_size = fread( source_str, 1, MAX_SOURCE_SIZE, fp);
  fclose( fp );


  // Use clGetPlatformIDs() to retrieve the number of platforms
  ret = clGetPlatformIDs(0, NULL, &ret_num_platforms);
  // Allocate enough space for each platform
  platform_id = (cl_platform_id*)malloc(ret_num_platforms * sizeof(cl_platform_id));
  // Fill in platforms with clGetPlatformIDs()
  ret = clGetPlatformIDs(ret_num_platforms, platform_id, NULL);


//ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);

//GetPlatformInfos
//-----------------------------------------------------
  size_t mem_space_for_info = 50;
  void* profile_ret = malloc(mem_space_for_info * sizeof(char));
  void* version_ret = malloc(mem_space_for_info * sizeof(char));
  void* name_ret = malloc(mem_space_for_info * sizeof(char));
  void* vendor_ret = malloc(mem_space_for_info * sizeof(char));

  ret = clGetPlatformInfo(platform_id[0],CL_PLATFORM_PROFILE,mem_space_for_info * sizeof(char),profile_ret,&size);//profile
  printf("PLATFORM_PROFILE = %s\n", (char*)profile_ret);
  printf("profile_ret size = %d\n\n", size);

  ret = clGetPlatformInfo(platform_id[0],CL_PLATFORM_VERSION,mem_space_for_info * sizeof(char),version_ret,&size);//version
  printf("PLATFORM_VERSION = %s\n", (char*)version_ret);
  printf("version_ret size = %d\n\n", size);

  ret = clGetPlatformInfo(platform_id[0],CL_PLATFORM_NAME,mem_space_for_info * sizeof(char),name_ret,&size);//name
  printf("PLATFORM_NAME = %s\n", (char*)name_ret);
  printf("name_ret size = %d\n\n", size);

  ret = clGetPlatformInfo(platform_id[0],CL_PLATFORM_VENDOR,mem_space_for_info * sizeof(char),vendor_ret,&size);//vendor
  printf("PLATFORM_VENDOR = %s\n", (char*)vendor_ret);
  printf("vendor_ret size = %d\n\n", size);

//-----------------------------------------------------


//clGetDeviceIDs
//-----------------------------------------------------
  ret = clGetDeviceIDs( platform_id[0], CL_DEVICE_TYPE_DEFAULT, 1, NULL, &ret_num_devices);//get available device number
  device_id = (cl_device_id*)malloc(sizeof(cl_device_id) * ret_num_devices);//allocate memory space
  ret = clGetDeviceIDs( platform_id[0], CL_DEVICE_TYPE_DEFAULT, ret_num_devices , device_id, NULL);//set devices info
//-----------------------------------------------------


//GetDeviceInfo
//-----------------------------------------------------
  void *info_ret1 = malloc(mem_space_for_info);
  ret = clGetDeviceInfo(device_id[0],CL_DEVICE_NAME,mem_space_for_info,info_ret1,&size);
  printf("DEVICE0_NAME = %s\n", (char*)info_ret1);
  printf("info_ret size = %d\n\n", size);


  void *info_ret2 = malloc(mem_space_for_info);
  ret = clGetDeviceInfo(device_id[0],CL_DEVICE_VENDOR,mem_space_for_info,info_ret2,&size);
  printf("DEVICE0_VENDOR = %s\n", (char*)info_ret2);
  printf("info_ret size = %d\n\n", size);


  void *info_ret3 = malloc(mem_space_for_info);
  ret = clGetDeviceInfo(device_id[1],CL_DEVICE_NAME,mem_space_for_info,info_ret3,&size);
  printf("DEVICE1_NAME = %s\n", (char*)info_ret3);
  printf("info_ret size = %d\n\n", size);


  void *info_ret4 = malloc(mem_space_for_info);
  ret = clGetDeviceInfo(device_id[1],CL_DEVICE_VENDOR_ID,mem_space_for_info,info_ret4,&size);
  printf("DEVICE1_VENDOR_ID = %d\n", *((cl_uint*)info_ret4));
  printf("info_ret size = %d\n\n", size);


  void *info_ret5 = malloc(mem_space_for_info);
  ret = clGetDeviceInfo(device_id[2],CL_DEVICE_PROFILE,mem_space_for_info,info_ret5,&size);
  printf("DEVICE2_PROFILE = %s\n", (char*)info_ret5);
  printf("info_ret size = %d\n\n", size);


  void *info_ret6 = malloc(mem_space_for_info);
  ret = clGetDeviceInfo(device_id[2],CL_DRIVER_VERSION,mem_space_for_info,info_ret6,&size);
  printf("DEVICE2_DRIVER_VERSION = %s\n", (char*)info_ret6);
  printf("info_ret size = %d\n\n", size);

//-----------------------------------------------------


  cl_context_properties properties[] =
  {
    CL_CONTEXT_PLATFORM,
    reinterpret_cast<cl_context_properties>(platform_id),
    0
  };

// create OpenCL context
  context = clCreateContext(properties, 1, device_id, NULL, NULL, &ret);

// context refcount
  ret = clRetainContext(context);
  ret = clRetainContext(context);

  ret = clReleaseContext(context);


//GetContextInfo
//-----------------------------------------------------
  void *info_ret7 = malloc(mem_space_for_info);
  ret = clGetContextInfo(context,CL_CONTEXT_REFERENCE_COUNT,mem_space_for_info,info_ret7,&size);
  printf("CONTEXT_REFERENCE_COUNT = %d\n", *((int*)info_ret7));
  printf("info_ret size = %d\n\n", size);



  void *info_ret8 = malloc(mem_space_for_info);
  ret = clGetContextInfo(context,CL_CONTEXT_PROPERTIES,mem_space_for_info,info_ret8,&size);
  printf("CONTEXT_PROPERTIES(platformID) = %d\n", *((int*)info_ret8));
  printf("info_ret size = %d\n\n", size);

//-----------------------------------------------------


  command_queue = clCreateCommandQueue(context, device_id[0], 0, &ret);


//GetCommandQueueInfo
//-----------------------------------------------------
  void *info_ret9 = malloc(mem_space_for_info);
  ret = clGetCommandQueueInfo(command_queue,CL_QUEUE_REFERENCE_COUNT,mem_space_for_info,info_ret9,&size);
  printf("QUEUE_REFERENCE_COUNT = %d\n", *((int*)info_ret9));
  printf("info_ret size = %d\n\n", size);



  void *info_ret10 = malloc(mem_space_for_info);
  ret = clGetCommandQueueInfo(command_queue,CL_QUEUE_PROPERTIES,mem_space_for_info,info_ret10,&size);
  printf("QUEUE_PROPERTIES = %d\n", *((int*)info_ret10));
  printf("info_ret size = %d\n\n", size);
//-----------------------------------------------------



  // memobj = clCreateBuffer(context, CL_MEM_READ_WRITE,MEM_SIZE * sizeof(char), NULL, &ret);

  cl_mem bufferA_core1; // Input array on the device
  cl_mem bufferB_core1; // Input array on the device
  cl_mem bufferC_core1; // Output array on the device

  cl_mem bufferA_core2; // Input array on the device
  cl_mem bufferB_core2; // Input array on the device
  cl_mem bufferC_core2; // Output array on the device

  cl_mem bufferA_core3; // Input array on the device
  cl_mem bufferB_core3; // Input array on the device
  cl_mem bufferC_core3; // Output array on the device

  cl_mem bufferA_core4; // Input array on the device
  cl_mem bufferB_core4; // Input array on the device
  cl_mem bufferC_core4; // Output array on the device

  // Use clCreateBuffer() to create a buffer object (d_A)
  // that will contain the data from the host array A
  bufferA_core1 = clCreateBuffer(
  context,
  CL_MEM_READ_ONLY,
  MEM_SIZE * sizeof(int),
  NULL,
  &ret);
  // Use clCreateBuffer() to create a buffer object (d_B)
  // that will contain the data from the host array B
  bufferB_core1 = clCreateBuffer(
  context,
  CL_MEM_READ_ONLY,
  MEM_SIZE * sizeof(int),
  NULL,
  &ret);
  // Use clCreateBuffer() to create a buffer object (d_C)
  // with enough space to hold the output data
  bufferC_core1 = clCreateBuffer(
  context,
  CL_MEM_WRITE_ONLY,
  MEM_SIZE * sizeof(int),
  NULL,
  &ret);

  bufferA_core2 = clCreateBuffer(
  context,
  CL_MEM_READ_ONLY,
  MEM_SIZE * sizeof(int),
  NULL,
  &ret);
  // Use clCreateBuffer() to create a buffer object (d_B)
  // that will contain the data from the host array B
  bufferB_core2 = clCreateBuffer(
  context,
  CL_MEM_READ_ONLY,
  MEM_SIZE * sizeof(int),
  NULL,
  &ret);
  // Use clCreateBuffer() to create a buffer object (d_C)
  // with enough space to hold the output data
  bufferC_core2 = clCreateBuffer(
  context,
  CL_MEM_WRITE_ONLY,
  MEM_SIZE * sizeof(int),
  NULL,
  &ret);

  bufferA_core3 = clCreateBuffer(
  context,
  CL_MEM_READ_ONLY,
  MEM_SIZE * sizeof(int),
  NULL,
  &ret);
  // Use clCreateBuffer() to create a buffer object (d_B)
  // that will contain the data from the host array B
  bufferB_core3 = clCreateBuffer(
  context,
  CL_MEM_READ_ONLY,
  MEM_SIZE * sizeof(int),
  NULL,
  &ret);
  // Use clCreateBuffer() to create a buffer object (d_C)
  // with enough space to hold the output data
  bufferC_core3 = clCreateBuffer(
  context,
  CL_MEM_WRITE_ONLY,
  MEM_SIZE * sizeof(int),
  NULL,
  &ret);

  bufferA_core4 = clCreateBuffer(
  context,
  CL_MEM_READ_ONLY,
  MEM_SIZE * sizeof(int),
  NULL,
  &ret);
  // Use clCreateBuffer() to create a buffer object (d_B)
  // that will contain the data from the host array B
  bufferB_core4 = clCreateBuffer(
  context,
  CL_MEM_READ_ONLY,
  MEM_SIZE * sizeof(int),
  NULL,
  &ret);
  // Use clCreateBuffer() to create a buffer object (d_C)
  // with enough space to hold the output data
  bufferC_core4 = clCreateBuffer(
  context,
  CL_MEM_WRITE_ONLY,
  MEM_SIZE * sizeof(int),
  NULL,
  &ret);
/*=============================================================
  divide OpenCL kernel data
  => specify the data range to execute and compile into hex file
  => two core with two hex files
===============================================================*/


  //------------------------------------------------------
    core_id_recieve = 0;
    fp2 = fopen("coreid.h", "w");
    fprintf(fp2,"#define get_global_id(n) 0\n");
    fprintf(fp2,"#define CORE_NUM 4");
    fclose(fp2);

    fp2 = fopen( "./LLVM/loader.c", "w" ) ;
    fprintf( fp2, "int main()  {\n" ) ;
    fprintf( fp2, "int size = %d;\n", MEM_SIZE ) ;
    fprintf( fp2, "int a [128];\n" ) ;
    fprintf( fp2, "__asm__ (\n" ) ;
    fprintf( fp2, "\"l.add      r3,r0,%0\\n\\t\"\n" ) ;
    fprintf( fp2, "\"l.add      r4,r0,%1\\n\\t\"\n" ) ;
    fprintf( fp2, "\"l.add      r5,r0,%2\\n\\t\"\n" ) ;
    fprintf( fp2, "\"l.add      r6,r0,%3\\n\\t\"\n" ) ;
    fprintf( fp2, "\"l.jal    cl_kernel\\n\\t\"\n" ) ;
    fprintf( fp2, "\"l.nop    \\n\\t\"\n") ;
    fprintf( fp2, "\"l.jr r0      \\n\\t\"\n" ) ;
    fprintf( fp2, "\"l.nop        \\n\"" ) ;
    fprintf( fp2, " :\n") ;
    fprintf( fp2, ": \"r\" (&size), \"r\" (%d), \"r\" (%d), \"r\" (%d) // input operands\n",   (131072+128*8)*4,  131072*4, (131072+128*4)*4 ) ;
    //                                                                                         C[0]                 A[0]        B[0]
    fprintf( fp2, ": );    // clobbered operands\n  return 0; \n  }" ) ;
    fclose(fp2);


    program = clCreateProgramWithSource(context, 1, (const char **)&source_str,
  				                      (const size_t *)&source_size, &ret);

    ret = clBuildProgram(program, 1, device_id, NULL, NULL, NULL);

    system("sudo mv ./test.hex ./test1.hex");
//------------------------------------------------------
//------------------------------------------------------
  core_id_recieve = 1;
  fp2 = fopen("coreid.h", "w");
  fprintf(fp2,"#define get_global_id(n) 0\n");
  fprintf(fp2,"#define CORE_NUM 4");
  fclose(fp2);

  fp2 = fopen( "./LLVM/loader.c", "w" ) ;
  fprintf( fp2, "int main()  {\n" ) ;
  fprintf( fp2, "int size = %d;\n", MEM_SIZE ) ;
  fprintf( fp2, "int a [128];\n" ) ;
  fprintf( fp2, "__asm__ (\n" ) ;
  fprintf( fp2, "\"l.add      r3,r0,%0\\n\\t\"\n" ) ;
  fprintf( fp2, "\"l.add      r4,r0,%1\\n\\t\"\n" ) ;
  fprintf( fp2, "\"l.add      r5,r0,%2\\n\\t\"\n" ) ;
  fprintf( fp2, "\"l.add      r6,r0,%3\\n\\t\"\n" ) ;
  fprintf( fp2, "\"l.jal    cl_kernel\\n\\t\"\n" ) ;
  fprintf( fp2, "\"l.nop    \\n\\t\"\n") ;
  fprintf( fp2, "\"l.jr r0      \\n\\t\"\n" ) ;
  fprintf( fp2, "\"l.nop        \\n\"" ) ;
  fprintf( fp2, " :\n") ;
  fprintf( fp2, ": \"r\" (&size), \"r\" (%d), \"r\" (%d), \"r\" (%d) // input operands\n",  (131072+128*9)*4,  (131072+128*1)*4, (131072+128*5)*4 ) ;
  //                                                                                         C[1]               A[1]             B[1]
  fprintf( fp2, ": );    // clobbered operands\n  return 0; \n  }" ) ;
  fclose(fp2);
  program = clCreateProgramWithSource(context, 1, (const char **)&source_str,
				                      (const size_t *)&source_size, &ret);

  ret = clBuildProgram(program, 1, device_id, NULL, NULL, NULL);

  system("sudo mv ./test.hex ./test2.hex");


  //------------------------------------------------------
  core_id_recieve = 2;
  fp2 = fopen("coreid.h", "w");
  fprintf(fp2,"#define get_global_id(n) 0\n");
  fprintf(fp2,"#define CORE_NUM 2");
  fclose(fp2);

  fp2 = fopen( "./LLVM/loader.c", "w" ) ;
  fprintf( fp2, "int main()  {\n" ) ;
  fprintf( fp2, "int size = %d;\n", MEM_SIZE ) ;
  fprintf( fp2, "int a [128];\n" ) ;
  fprintf( fp2, "__asm__ (\n" ) ;
  fprintf( fp2, "\"l.add      r3,r0,%0\\n\\t\"\n" ) ;
  fprintf( fp2, "\"l.add      r4,r0,%1\\n\\t\"\n" ) ;
  fprintf( fp2, "\"l.add      r5,r0,%2\\n\\t\"\n" ) ;
  fprintf( fp2, "\"l.add      r6,r0,%3\\n\\t\"\n" ) ;
  fprintf( fp2, "\"l.jal    cl_kernel\\n\\t\"\n" ) ;
  fprintf( fp2, "\"l.nop    \\n\\t\"\n") ;
  fprintf( fp2, "\"l.jr r0      \\n\\t\"\n" ) ;
  fprintf( fp2, "\"l.nop        \\n\"" ) ;
  fprintf( fp2, " :\n") ;
  fprintf( fp2, ": \"r\" (&size), \"r\" (%d), \"r\" (%d), \"r\" (%d) // input operands\n", (131072+128*10)*4,  (131072+128*2)*4, (131072+128*6)*4  ) ;
  //                                                                                         C[2]               A[2]             B[2]
  fprintf( fp2, ": );    // clobbered operands\n  return 0; \n  }" ) ;
  fclose(fp2);


  program = clCreateProgramWithSource(context, 1, (const char **)&source_str,
				                      (const size_t *)&source_size, &ret);

  ret = clBuildProgram(program, 1, device_id, NULL, NULL, NULL);

  system("sudo mv ./test.hex ./test3.hex");

  //------------------------------------------------------
  core_id_recieve = 3;
  fp2 = fopen("coreid.h", "w");
  fprintf(fp2,"#define get_global_id(n) 0\n");
  fprintf(fp2,"#define CORE_NUM 4");
  fclose(fp2);

  fp2 = fopen( "./LLVM/loader.c", "w" ) ;
  fprintf( fp2, "int main()  {\n" ) ;
  fprintf( fp2, "int size = %d;\n", MEM_SIZE ) ;
  fprintf( fp2, "int a [128];\n" ) ;
  fprintf( fp2, "__asm__ (\n" ) ;
  fprintf( fp2, "\"l.add      r3,r0,%0\\n\\t\"\n" ) ;
  fprintf( fp2, "\"l.add      r4,r0,%1\\n\\t\"\n" ) ;
  fprintf( fp2, "\"l.add      r5,r0,%2\\n\\t\"\n" ) ;
  fprintf( fp2, "\"l.add      r6,r0,%3\\n\\t\"\n" ) ;
  fprintf( fp2, "\"l.jal    cl_kernel\\n\\t\"\n" ) ;
  fprintf( fp2, "\"l.nop    \\n\\t\"\n") ;
  fprintf( fp2, "\"l.jr r0      \\n\\t\"\n" ) ;
  fprintf( fp2, "\"l.nop        \\n\"" ) ;
  fprintf( fp2, " :\n") ;
  fprintf( fp2, ": \"r\" (&size), \"r\" (%d), \"r\" (%d), \"r\" (%d) // input operands\n",  (131072+128*11)*4,  (131072+128*3)*4, (131072+128*7)*4  ) ;
  //                                                                                         C[3]               A[3]             B[3]
  fprintf( fp2, ": );    // clobbered operands\n  return 0; \n  }" ) ;
  fclose(fp2);


  program = clCreateProgramWithSource(context, 1, (const char **)&source_str,
				                      (const size_t *)&source_size, &ret);

  ret = clBuildProgram(program, 1, device_id, NULL, NULL, NULL);

  system("sudo mv ./test.hex ./test4.hex");

  //----------------------------------

/*=============================================================
  => program hex file
  => start core execution (time measurement is here)
  => recieve data
===============================================================*/
  kernel = clCreateKernel(program, fileName, &ret);

  // Use clEnqueueWriteBuffer() to write input array A to
  // the device buffer bufferA_core1
  core_id_recieve = 0;
  ret = clEnqueueWriteBuffer( command_queue, NULL, CL_FALSE,
  131072, // offset: 0 ~ 65535 local, 65536 ~ 131071 C2C, 131072 ~ global
  (MEM_SIZE*4)*2, &A, 0, NULL, NULL);

  ret = clEnqueueWriteBuffer( command_queue,  NULL,  CL_FALSE,
  131072+MEM_SIZE*4, // offset: 0 ~ 65535 local, 65536 ~ 131071 C2C, 131072 ~ global
  (MEM_SIZE*4)*2, &B, 0, NULL, NULL);


  ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&memobj);

  gettimeofday(&start,NULL);//start time

  ret = clEnqueueTask(command_queue, kernel, 0, NULL,NULL);

  gettimeofday(&end,NULL);//end time


  // print reult
  for ( int k = 0 ; k < 4 ; k++  ) {
    core_id_recieve = i ;
    ret = clEnqueueReadBuffer(command_queue,bufferC_core1, CL_TRUE,131072+(128*(8+k)),
			                128*2, ReadBuffer, 0, NULL, NULL);
    printf("============================================================================\n");
    printf("\nResult : \n");
    for(i=0; i<128; i++) {
      string[i] = ( ReadBuffer[2*i+1] << sizeof(unsigned short)*8 ) + ReadBuffer[2*i];
      //string[i] = ( ReadBuffer[2*i] << sizeof(unsigned short)*8 ) + ReadBuffer[2*i+1] ;
	    printf("%x\t",string[i]);
	    if(i%8 == 7) printf("\n");
    }  // for
    printf("\n============================================================================\n");
  }  // for


  exec_time  = 1000000 * ( end.tv_sec - start.tv_sec ) + end.tv_usec - start.tv_usec;
  exec_time /= 1000000;
  printf("Execution time with 2 cores:   %f  sec\n",exec_time);

  Baton_Close();

  free(platform_id);
  free(device_id);
  free(source_str);

//platform_info
//---------------------
  free(profile_ret);
  free(version_ret);
  free(name_ret);
  free(vendor_ret);
//---------------------

//device_info
//---------------------
  free(info_ret1);
  free(info_ret2);
  free(info_ret3);
  free(info_ret4);
  free(info_ret5);
  free(info_ret6);
//---------------------

//context_info
//---------------------
  free(info_ret7);
  free(info_ret8);

//commandqueue_info
//---------------------
  free(info_ret9);
  free(info_ret10);

  return 0;
}
